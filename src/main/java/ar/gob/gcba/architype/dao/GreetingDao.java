package ar.gob.gcba.architype.dao;

import ar.gob.gcba.architype.exception.CustomException;
import ar.gob.gcba.architype.model.Greeting;

import java.util.List;

public interface GreetingDao {


    Greeting saveGreeting(Greeting greeting) throws CustomException;

    List<Greeting> getAllGreeting() throws CustomException;

}
