package ar.gob.gcba.architype.dao.impl;

import ar.gob.gcba.architype.dao.GreetingDao;
import ar.gob.gcba.architype.exception.CustomException;
import ar.gob.gcba.architype.model.Greeting;
import ar.gob.gcba.architype.repository.GreetingJpaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository("greetingDao")
public class GreetingDaoImpl implements GreetingDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(GreetingDaoImpl.class);

    @Autowired @Qualifier("greetingJpaRepository")
    private GreetingJpaRepository greetingJpaRepository;


    @Override
    public Greeting saveGreeting(Greeting greeting) throws CustomException{
        try {
            return greetingJpaRepository.save(greeting);
        }catch (Exception e){
            LOGGER.error("Catch on save greeting");
            throw new CustomException("Error triying save greeting",e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public List<Greeting> getAllGreeting() throws CustomException {
        try {
            return greetingJpaRepository.findAll();
        }catch (Exception e){
            LOGGER.error("Catch on getAllGreeting");
            throw new CustomException("Error triying get all Greeting",e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
