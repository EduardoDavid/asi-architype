package ar.gob.gcba.architype.exception;

import ar.gob.gcba.architype.dto.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestResponseExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestResponseExceptionHandler.class);

    @ExceptionHandler(CustomException.class)
    public ResponseEntity handleCustomException(CustomException e){

        LOGGER.error(e.getMessage(),e);
        String message = e.getMessage();
        HttpStatus httpStatus = e.getHttpStatus() != null ? e.getHttpStatus() : HttpStatus.INTERNAL_SERVER_ERROR;
        return ResponseEntity.status(httpStatus).body(new RestResponse(httpStatus, message));
    }
}
