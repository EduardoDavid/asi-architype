package ar.gob.gcba.architype.controller;


import ar.gob.gcba.architype.dao.GreetingDao;
import ar.gob.gcba.architype.exception.CustomException;
import ar.gob.gcba.architype.model.Greeting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GreetingController {

    @Autowired @Qualifier("greetingDao")
    private GreetingDao greetingDao;



    @GetMapping("/greeting")
    public ResponseEntity getAllGreeting() throws CustomException {

        List<Greeting> allGreeting = greetingDao.getAllGreeting();

        return new ResponseEntity(allGreeting, HttpStatus.OK);
    }

    @PostMapping("/greeting")
    public ResponseEntity saveGreeting(@RequestBody String content) throws CustomException {

        return new ResponseEntity(greetingDao.saveGreeting(new Greeting(content)), HttpStatus.OK);
    }

}
