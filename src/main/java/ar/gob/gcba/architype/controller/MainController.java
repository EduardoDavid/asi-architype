package ar.gob.gcba.architype.controller;

import ar.gob.gcba.architype.dto.SuccessRestResponse;
import ar.gob.gcba.architype.exception.CustomException;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@Api(tags = {"Persons Service"})
public class MainController {


    @ApiIgnore
    @GetMapping("/apidoc")
    public ModelAndView redirectToSwaggerUI() {
        return new ModelAndView("redirect:/architype-ui.html");
    }


    @GetMapping("server-test")
    public ResponseEntity<SuccessRestResponse> serverTest(){

        ResponseEntity<SuccessRestResponse> response;
        response = new ResponseEntity<>(new SuccessRestResponse(HttpStatus.OK, "VIVA LA PEPA"), HttpStatus.OK);
        return response;

    }


    @GetMapping("server-test-error")
    public ResponseEntity<SuccessRestResponse> serverErrorTest() throws CustomException {
        try {
            Integer a = 0 / 0;
            return null;
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }
}
