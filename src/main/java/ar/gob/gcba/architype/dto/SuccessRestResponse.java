package ar.gob.gcba.architype.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SuccessRestResponse extends RestResponse implements Serializable {

    private static final long serialVersionUID = -6295324801768190127L;
    private Object body;

    public SuccessRestResponse(HttpStatus httpStatus, Object body) {
        super(httpStatus,body);
        this.body = body;
    }
}
