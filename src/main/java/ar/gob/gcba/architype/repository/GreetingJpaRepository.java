package ar.gob.gcba.architype.repository;

import ar.gob.gcba.architype.model.Greeting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("greetingJpaRepository")
public interface GreetingJpaRepository extends JpaRepository<Greeting,Long> {
}
