package ar.gob.gcba.architype;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AsiArchitypeApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(AsiArchitypeApplication.class);

    public static void main(String[] args) {
      SpringApplication.run(AsiArchitypeApplication.class, args);
        LOGGER.info("Swagger Application running...");
    }
}
