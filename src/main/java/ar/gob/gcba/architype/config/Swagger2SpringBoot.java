package ar.gob.gcba.architype.config;

import ar.gob.gcba.architype.controller.MainController;
import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.singletonList;
import static org.springframework.http.HttpStatus.*;


@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackageClasses = {
        MainController.class
})
public class Swagger2SpringBoot {

    public static void main(String[] args) {
        SpringApplication.run(Swagger2SpringBoot.class, args);
    }


    private ApiInfo apiInfo() {
        ApiInfo apiInfo;
        apiInfo = new ApiInfo(
                "GCBA ASI - SATPDI API :: Spring Boot REST API",
                "Sistema de Agilizacion de Tramites a PDI (SATPDI)",
                "1",
                "Terms of service",
                new Contact("ASI Systems Development", "https://asijira-confluence.buenosaires.gob.ar/pages/viewpage.action?pageId=30507277", "pwilczek@buenosaires.gob.ar"),
                "Apache License Version 2.0",
                "https://www.apache.org/licenses/LICENSE-2.0", Collections.emptyList());
        return apiInfo;
    }

    @Bean
    public Docket petApi() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("ar.gob.gcba.architype.controller"))
                .build()
                .enable(true)
                .apiInfo(apiInfo())
                .securityContexts(newArrayList(securityContext()))
                .securitySchemes(newArrayList(apiKey()));

        setTags(docket);
        setGlobalsResponseMessage(docket);

        return docket;
    }

    private void setTags(Docket docket) {
        docket.tags(new Tag("Persons Service", "Servicio para las personas",1));
    }

    private void setGlobalsResponseMessage(Docket docket) {
        docket.globalResponseMessage(RequestMethod.GET,
                newArrayList(
                    new ResponseMessageBuilder().code(BAD_REQUEST.value()).message(BAD_REQUEST.toString()).build(),
                    new ResponseMessageBuilder().code(FORBIDDEN.value()).message(FORBIDDEN.toString()).build(),
                    new ResponseMessageBuilder().code(NOT_FOUND.value()).message(NOT_FOUND.toString()).build(),
                    new ResponseMessageBuilder().code(CONFLICT.value()).message(CONFLICT.toString()).build(),
                    new ResponseMessageBuilder().code(INTERNAL_SERVER_ERROR.value()).message(INTERNAL_SERVER_ERROR.toString()).build()
                        )
        );
    }

    @Autowired
    private TypeResolver typeResolver;

    private ApiKey apiKey() {
        return new ApiKey("mykey", "api_key", "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.regex("/anyPath.*"))
                .build();
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope
                = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return singletonList(
                new SecurityReference("mykey", authorizationScopes));
    }

    @Bean
    SecurityConfiguration security() {
        return SecurityConfigurationBuilder.builder()
                .clientId("test-app-client-id")
                .clientSecret("test-app-client-secret")
                .realm("test-app-realm")
                .appName("test-app")
                .scopeSeparator(",")
                .additionalQueryStringParams(null)
                .useBasicAuthenticationWithAccessCodeGrant(false)
                .build();
    }

    @Bean
    UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder()
                .deepLinking(true)
                .displayOperationId(false)
                .defaultModelsExpandDepth(1)
                .defaultModelExpandDepth(1)
                .defaultModelRendering(ModelRendering.EXAMPLE)
                .displayRequestDuration(false)
                .docExpansion(DocExpansion.NONE)
                .filter(false)
                .maxDisplayedTags(null)
                .operationsSorter(OperationsSorter.ALPHA)
                .showExtensions(false)
                .tagsSorter(TagsSorter.ALPHA)
                .supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS)
                .validatorUrl(null)
                .build();
    }

}