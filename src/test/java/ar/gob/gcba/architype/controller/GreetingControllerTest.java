package ar.gob.gcba.architype.controller;


import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GreetingControllerTest {

    @Autowired
    private  MockMvc mockMvc;

    @Test
    public void test1SaveGreeting() throws Exception {

        mockMvc.perform(post("/greeting")
                .contentType(MediaType.APPLICATION_JSON)
                .content("hola mundo"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("hola")));
    }

    @Test
    public void test2GetAllGreetingTest() throws Exception {

        mockMvc.perform(get("/greeting")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"id\":1,\"content\":\"hola mundo\"}")))
                .andReturn();

    }
}
